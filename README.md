# Efrei-expo-2019

## Members:
- Jean-Charles Moussé
- Andrea Serrano
- Louis Adam

## Requirements
- [X] Use GraphQL API, Expo, React Navigation
- [X] Users CRUD
- [X] Posts CRUD
- [] Add Camera handling for Posts
- [X] Display Posts list
- [] Filter Posts list
- [X] Display Post details
- [X] Display User details, with the list of his own Posts
- [X] Only needs `npm install && expo start` to start
- [] Publish project on Expo, using unique `expo.slug` identifier
- [] Publish API on Heroku : 
