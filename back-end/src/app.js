import { GraphQLServer } from 'graphql-yoga';
import { userSchema, itemSchema, baseSchema } from './schema';
import db from './database'
import users from './database/mock/users';
import items from './database/mock/items';
import { User, Item } from './database/models';
import { makeExecutableSchema } from 'graphql-tools';
import { userResolver, itemResolver } from './resolvers';

const schemas = makeExecutableSchema({
    typeDefs: [baseSchema, userSchema, itemSchema],
    resolvers: [userResolver, itemResolver],
});

const app = new GraphQLServer({ schema: schemas });

const env = process.env.NODE_ENV || 'development';
const PORT = process.env.PORT || 3000;

db.authenticate()
    .then(async () => {
        console.log('Connection has been established successfully.');
        if (env === 'production') {
            await db.sync({ alter: true });
        } else {
            await db.sync({ force: true });
        }

        // TODO: Remvoe this mock init ?
        // await User.bulkCreate(users);
        // await Item.bulkCreate(items);

        app.start({ port: PORT }, () => {
            console.log(`Server started on port ${PORT} => http://localhost:${PORT}`)
        });
    })
    .catch(err => console.log('Connection to the database has failed.', err));