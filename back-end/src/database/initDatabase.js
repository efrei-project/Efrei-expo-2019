import Sequelize from 'sequelize';
import { User, Item } from './models';

const env = process.env.NODE_ENV || 'development';
const config = require('./sequelize.config.js')[env];

const db = env === 'development'
    ? new Sequelize(
        config.database,
        config.username,
        config.password,
        config,
    )
    : new Sequelize(config.databaseUrl, config);

Item.init(db);
User.init(db);

Item.associate();

export default db;