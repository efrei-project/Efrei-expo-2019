import users from './users';
import items from './items';

export default {
    users,
    items
}
