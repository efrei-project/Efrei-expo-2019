export default [
    {
        id: '3a67eb57-d3ae-11e9-aec2-172a4627723d',
        name: 'Gorgeous Concrete Cheese',
        description: 'A great product description here !',
        price: '567.00',
        userId: '3a672800-d3ae-11e9-aec2-172a4627723d',
        imageUrl: 'http://lorempixel.com/640/480',
        category: 'Health'
    },
    {
        id: '3a67eb58-d3ae-11e9-aec2-172a4627723d',
        name: 'Small Concrete Ball',
        description: 'A great product description here !',
        price: '145.00',
        userId: '3a672800-d3ae-11e9-aec2-172a4627723d',
        imageUrl: 'http://lorempixel.com/640/480',
        category: 'Games'
    },
    {
        id: '3a67eb59-d3ae-11e9-aec2-172a4627723d',
        name: 'Unbranded Frozen Hat',
        description: 'A great product description here !',
        price: '971.00',
        userId: '3a67eb54-d3ae-11e9-aec2-172a4627723d',
        imageUrl: 'http://lorempixel.com/640/480',
        category: 'Toys'
    },
    {
        id: '3a67eb5a-d3ae-11e9-aec2-172a4627723d',
        name: 'Fantastic Steel Chicken',
        description: 'A great product description here !',
        price: '757.00',
        userId: '3a679d34-d3ae-11e9-aec2-172a4627723d',
        imageUrl: 'http://lorempixel.com/640/480',
        category: 'Shoes'
    },
    {
        id: '3a67eb5b-d3ae-11e9-aec2-172a4627723d',
        name: 'Tasty Rubber Salad',
        description: 'A great product description here !',
        price: '848.00',
        userId: '3a672800-d3ae-11e9-aec2-172a4627723d',
        imageUrl: 'http://lorempixel.com/640/480',
        category: 'Movies'
    },
    {
        id: '3a67eb5c-d3ae-11e9-aec2-172a4627723d',
        name: 'Intelligent Steel Chicken',
        description: 'A great product description here !',
        price: '869.00',
        userId: '3a67762a-d3ae-11e9-aec2-172a4627723d',
        imageUrl: 'http://lorempixel.com/640/480',
        category: 'Toys'
    },
    {
        id: '3a67eb5d-d3ae-11e9-aec2-172a4627723d',
        name: 'Handmade Plastic Hat',
        description: 'A great product description here !',
        price: '999.00',
        userId: '3a677626-d3ae-11e9-aec2-172a4627723d',
        imageUrl: 'http://lorempixel.com/640/480',
        category: 'Baby'
    },
    {
        id: '3a67eb5e-d3ae-11e9-aec2-172a4627723d',
        name: 'Fantastic Fresh Soap',
        description: 'A great product description here !',
        price: '720.00',
        userId: '3a67c44b-d3ae-11e9-aec2-172a4627723d',
        imageUrl: 'http://lorempixel.com/640/480',
        category: 'Toys'
    },
    {
        id: '3a67eb5f-d3ae-11e9-aec2-172a4627723d',
        name: 'Refined Concrete Gloves',
        description: 'A great product description here !',
        price: '346.00',
        userId: '3a672800-d3ae-11e9-aec2-172a4627723d',
        imageUrl: 'http://lorempixel.com/640/480',
        category: 'Industrial'
    },
    {
        id: '3a67eb60-d3ae-11e9-aec2-172a4627723d',
        name: 'Practical Cotton Gloves',
        description: 'A great product description here !',
        price: '495.00',
        userId: '3a67c444-d3ae-11e9-aec2-172a4627723d',
        imageUrl: 'http://lorempixel.com/640/480',
        category: 'Garden'
    },
    {
        id: '3a67eb61-d3ae-11e9-aec2-172a4627723d',
        name: 'Small Wooden Towels',
        description: 'A great product description here !',
        price: '80.00',
        userId: '3a679d3a-d3ae-11e9-aec2-172a4627723d',
        imageUrl: 'http://lorempixel.com/640/480',
        category: 'Home'
    },
    {
        id: '3a67eb62-d3ae-11e9-aec2-172a4627723d',
        name: 'Ergonomic Soft Cheese',
        description: 'A great product description here !',
        price: '919.00',
        userId: '3a679d35-d3ae-11e9-aec2-172a4627723d',
        imageUrl: 'http://lorempixel.com/640/480',
        category: 'Grocery'
    },
    {
        id: '3a67eb63-d3ae-11e9-aec2-172a4627723d',
        name: 'Practical Frozen Cheese',
        description: 'A great product description here !',
        price: '443.00',
        userId: '3a67c441-d3ae-11e9-aec2-172a4627723d',
        imageUrl: 'http://lorempixel.com/640/480',
        category: 'Clothing'
    },
    {
        id: '3a67eb64-d3ae-11e9-aec2-172a4627723d',
        name: 'Gorgeous Wooden Table',
        description: 'A great product description here !',
        price: '594.00',
        userId: '3a677624-d3ae-11e9-aec2-172a4627723d',
        imageUrl: 'http://lorempixel.com/640/480',
        category: 'Garden'
    },
    {
        id: '3a67eb65-d3ae-11e9-aec2-172a4627723d',
        name: 'Incredible Cotton Chicken',
        description: 'A great product description here !',
        price: '472.00',
        userId: '3a67c44b-d3ae-11e9-aec2-172a4627723d',
        imageUrl: 'http://lorempixel.com/640/480',
        category: 'Books'
    },
    {
        id: '3a67eb66-d3ae-11e9-aec2-172a4627723d',
        name: 'Incredible Plastic Chicken',
        description: 'A great product description here !',
        price: '536.00',
        userId: '3a67eb52-d3ae-11e9-aec2-172a4627723d',
        imageUrl: 'http://lorempixel.com/640/480',
        category: 'Movies'
    },
    {
        id: '3a67eb67-d3ae-11e9-aec2-172a4627723d',
        name: 'Tasty Frozen Table',
        description: 'A great product description here !',
        price: '223.00',
        userId: '3a67eb53-d3ae-11e9-aec2-172a4627723d',
        imageUrl: 'http://lorempixel.com/640/480',
        category: 'Sports'
    },
    {
        id: '3a67eb68-d3ae-11e9-aec2-172a4627723d',
        name: 'Handmade Rubber Chair',
        description: 'A great product description here !',
        price: '52.00',
        userId: '3a679d3b-d3ae-11e9-aec2-172a4627723d',
        imageUrl: 'http://lorempixel.com/640/480',
        category: 'Garden'
    },
    {
        id: '3a67eb69-d3ae-11e9-aec2-172a4627723d',
        name: 'Small Concrete Shirt',
        description: 'A great product description here !',
        price: '250.00',
        userId: '3a679d38-d3ae-11e9-aec2-172a4627723d',
        imageUrl: 'http://lorempixel.com/640/480',
        category: 'Computers'
    },
    {
        id: '3a67eb6a-d3ae-11e9-aec2-172a4627723d',
        name: 'Handmade Soft Shirt',
        description: 'A great product description here !',
        price: '207.00',
        userId: '3a67c449-d3ae-11e9-aec2-172a4627723d',
        imageUrl: 'http://lorempixel.com/640/480',
        category: 'Books'
    },
    {
        id: '3a67eb6b-d3ae-11e9-aec2-172a4627723d',
        name: 'Handcrafted Cotton Car',
        description: 'A great product description here !',
        price: '648.00',
        userId: '3a67c447-d3ae-11e9-aec2-172a4627723d',
        imageUrl: 'http://lorempixel.com/640/480',
        category: 'Music'
    },
    {
        id: '3a67eb6c-d3ae-11e9-aec2-172a4627723d',
        name: 'Practical Metal Table',
        description: 'A great product description here !',
        price: '604.00',
        userId: '3a677629-d3ae-11e9-aec2-172a4627723d',
        imageUrl: 'http://lorempixel.com/640/480',
        category: 'Music'
    },
    {
        id: '3a67eb6d-d3ae-11e9-aec2-172a4627723d',
        name: 'Tasty Metal Ball',
        description: 'A great product description here !',
        price: '14.00',
        userId: '3a679d3d-d3ae-11e9-aec2-172a4627723d',
        imageUrl: 'http://lorempixel.com/640/480',
        category: 'Tools'
    },
    {
        id: '3a67eb6e-d3ae-11e9-aec2-172a4627723d',
        name: 'Licensed Wooden Mouse',
        description: 'A great product description here !',
        price: '342.00',
        userId: '3a67c442-d3ae-11e9-aec2-172a4627723d',
        imageUrl: 'http://lorempixel.com/640/480',
        category: 'Movies'
    },
    {
        id: '3a67eb6f-d3ae-11e9-aec2-172a4627723d',
        name: 'Refined Cotton Tuna',
        description: 'A great product description here !',
        price: '479.00',
        userId: '3a679d34-d3ae-11e9-aec2-172a4627723d',
        imageUrl: 'http://lorempixel.com/640/480',
        category: 'Movies'
    },
    {
        id: '3a67eb70-d3ae-11e9-aec2-172a4627723d',
        name: 'Handmade Steel Fish',
        description: 'A great product description here !',
        price: '384.00',
        userId: '3a67c44f-d3ae-11e9-aec2-172a4627723d',
        imageUrl: 'http://lorempixel.com/640/480',
        category: 'Beauty'
    },
    {
        id: '3a67eb71-d3ae-11e9-aec2-172a4627723d',
        name: 'Tasty Soft Bike',
        description: 'A great product description here !',
        price: '212.00',
        userId: '3a677624-d3ae-11e9-aec2-172a4627723d',
        imageUrl: 'http://lorempixel.com/640/480',
        category: 'Movies'
    },
    {
        id: '3a67eb72-d3ae-11e9-aec2-172a4627723d',
        name: 'Refined Granite Shoes',
        description: 'A great product description here !',
        price: '304.00',
        userId: '3a67c444-d3ae-11e9-aec2-172a4627723d',
        imageUrl: 'http://lorempixel.com/640/480',
        category: 'Health'
    },
    {
        id: '3a67eb73-d3ae-11e9-aec2-172a4627723d',
        name: 'Ergonomic Wooden Fish',
        description: 'A great product description here !',
        price: '713.00',
        userId: '3a679d37-d3ae-11e9-aec2-172a4627723d',
        imageUrl: 'http://lorempixel.com/640/480',
        category: 'Movies'
    },
    {
        id: '3a67eb74-d3ae-11e9-aec2-172a4627723d',
        name: 'Generic Concrete Cheese',
        description: 'A great product description here !',
        price: '502.00',
        userId: '3a67c447-d3ae-11e9-aec2-172a4627723d',
        imageUrl: 'http://lorempixel.com/640/480',
        category: 'Jewelery'
    },
    {
        id: '3a681260-d3ae-11e9-aec2-172a4627723d',
        name: 'Fantastic Frozen Bacon',
        description: 'A great product description here !',
        price: '794.00',
        userId: '3a679d3b-d3ae-11e9-aec2-172a4627723d',
        imageUrl: 'http://lorempixel.com/640/480',
        category: 'Jewelery'
    },
    {
        id: '3a681261-d3ae-11e9-aec2-172a4627723d',
        name: 'Awesome Cotton Pizza',
        description: 'A great product description here !',
        price: '795.00',
        userId: '3a677625-d3ae-11e9-aec2-172a4627723d',
        imageUrl: 'http://lorempixel.com/640/480',
        category: 'Tools'
    },
    {
        id: '3a681262-d3ae-11e9-aec2-172a4627723d',
        name: 'Refined Rubber Computer',
        description: 'A great product description here !',
        price: '645.00',
        userId: '3a679d3c-d3ae-11e9-aec2-172a4627723d',
        imageUrl: 'http://lorempixel.com/640/480',
        category: 'Health'
    },
    {
        id: '3a681263-d3ae-11e9-aec2-172a4627723d',
        name: 'Practical Rubber Tuna',
        description: 'A great product description here !',
        price: '161.00',
        userId: '3a67c44b-d3ae-11e9-aec2-172a4627723d',
        imageUrl: 'http://lorempixel.com/640/480',
        category: 'Industrial'
    },
    {
        id: '3a681264-d3ae-11e9-aec2-172a4627723d',
        name: 'Incredible Cotton Bike',
        description: 'A great product description here !',
        price: '509.00',
        userId: '3a67c449-d3ae-11e9-aec2-172a4627723d',
        imageUrl: 'http://lorempixel.com/640/480',
        category: 'Electronics'
    },
    {
        id: '3a681265-d3ae-11e9-aec2-172a4627723d',
        name: 'Awesome Cotton Cheese',
        description: 'A great product description here !',
        price: '293.00',
        userId: '3a679d31-d3ae-11e9-aec2-172a4627723d',
        imageUrl: 'http://lorempixel.com/640/480',
        category: 'Shoes'
    },
    {
        id: '3a681266-d3ae-11e9-aec2-172a4627723d',
        name: 'Handcrafted Plastic Soap',
        description: 'A great product description here !',
        price: '365.00',
        userId: '3a67c44a-d3ae-11e9-aec2-172a4627723d',
        imageUrl: 'http://lorempixel.com/640/480',
        category: 'Shoes'
    },
    {
        id: '3a681267-d3ae-11e9-aec2-172a4627723d',
        name: 'Tasty Soft Chair',
        description: 'A great product description here !',
        price: '264.00',
        userId: '3a677621-d3ae-11e9-aec2-172a4627723d',
        imageUrl: 'http://lorempixel.com/640/480',
        category: 'Jewelery'
    },
    {
        id: '3a681268-d3ae-11e9-aec2-172a4627723d',
        name: 'Awesome Rubber Pizza',
        description: 'A great product description here !',
        price: '679.00',
        userId: '3a67c44c-d3ae-11e9-aec2-172a4627723d',
        imageUrl: 'http://lorempixel.com/640/480',
        category: 'Jewelery'
    },
    {
        id: '3a681269-d3ae-11e9-aec2-172a4627723d',
        name: 'Handcrafted Cotton Shirt',
        description: 'A great product description here !',
        price: '986.00',
        userId: '3a67c447-d3ae-11e9-aec2-172a4627723d',
        imageUrl: 'http://lorempixel.com/640/480',
        category: 'Shoes'
    },
    {
        id: '3a68126a-d3ae-11e9-aec2-172a4627723d',
        name: 'Small Frozen Pizza',
        description: 'A great product description here !',
        price: '48.00',
        userId: '3a679d3a-d3ae-11e9-aec2-172a4627723d',
        imageUrl: 'http://lorempixel.com/640/480',
        category: 'Movies'
    },
    {
        id: '3a68126b-d3ae-11e9-aec2-172a4627723d',
        name: 'Small Frozen Fish',
        description: 'A great product description here !',
        price: '959.00',
        userId: '3a677620-d3ae-11e9-aec2-172a4627723d',
        imageUrl: 'http://lorempixel.com/640/480',
        category: 'Grocery'
    },
    {
        id: '3a68126c-d3ae-11e9-aec2-172a4627723d',
        name: 'Rustic Fresh Car',
        description: 'A great product description here !',
        price: '376.00',
        userId: '3a677624-d3ae-11e9-aec2-172a4627723d',
        imageUrl: 'http://lorempixel.com/640/480',
        category: 'Music'
    },
    {
        id: '3a68126d-d3ae-11e9-aec2-172a4627723d',
        name: 'Generic Concrete Soap',
        description: 'A great product description here !',
        price: '815.00',
        userId: '3a679d3a-d3ae-11e9-aec2-172a4627723d',
        imageUrl: 'http://lorempixel.com/640/480',
        category: 'Toys'
    },
    {
        id: '3a68126e-d3ae-11e9-aec2-172a4627723d',
        name: 'Intelligent Soft Car',
        description: 'A great product description here !',
        price: '598.00',
        userId: '3a679d37-d3ae-11e9-aec2-172a4627723d',
        imageUrl: 'http://lorempixel.com/640/480',
        category: 'Kids'
    },
    {
        id: '3a68126f-d3ae-11e9-aec2-172a4627723d',
        name: 'Incredible Steel Salad',
        description: 'A great product description here !',
        price: '275.00',
        userId: '3a67c441-d3ae-11e9-aec2-172a4627723d',
        imageUrl: 'http://lorempixel.com/640/480',
        category: 'Computers'
    },
    {
        id: '3a681270-d3ae-11e9-aec2-172a4627723d',
        name: 'Sleek Cotton Soap',
        description: 'A great product description here !',
        price: '304.00',
        userId: '3a67eb52-d3ae-11e9-aec2-172a4627723d',
        imageUrl: 'http://lorempixel.com/640/480',
        category: 'Books'
    },
    {
        id: '3a681271-d3ae-11e9-aec2-172a4627723d',
        name: 'Intelligent Soft Towels',
        description: 'A great product description here !',
        price: '710.00',
        userId: '3a679d3e-d3ae-11e9-aec2-172a4627723d',
        imageUrl: 'http://lorempixel.com/640/480',
        category: 'Kids'
    },
    {
        id: '3a681272-d3ae-11e9-aec2-172a4627723d',
        name: 'Handmade Cotton Keyboard',
        description: 'A great product description here !',
        price: '796.00',
        userId: '3a67eb53-d3ae-11e9-aec2-172a4627723d',
        imageUrl: 'http://lorempixel.com/640/480',
        category: 'Automotive'
    },
    {
        id: '3a681273-d3ae-11e9-aec2-172a4627723d',
        name: 'Tasty Frozen Cheese',
        description: 'A great product description here !',
        price: '986.00',
        userId: '3a67c447-d3ae-11e9-aec2-172a4627723d',
        imageUrl: 'http://lorempixel.com/640/480',
        category: 'Toys'
    }
];
