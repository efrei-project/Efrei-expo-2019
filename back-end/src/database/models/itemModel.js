import Sequelize, { Model, DataTypes } from 'sequelize';
import {User} from '../models';

class Item extends Model {
    static init(database) {
        return super.init(
            {
                id: {
                    type: DataTypes.UUID,
                    primaryKey: true,
                    defaultValue: Sequelize.UUIDV4,
                },
                name: DataTypes.STRING,
                description: DataTypes.STRING,
                price: DataTypes.FLOAT,
                imageUrl: DataTypes.STRING,
                category: DataTypes.STRING
            }, {
                tableName: 'Item',
                sequelize: database,
            },
        );
    }
}

Item.associate = () => {
    Item.belongsTo(User, { foreignKey: 'userId'});
};

export default Item;
