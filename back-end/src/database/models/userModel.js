import Sequelize, { Model, DataTypes } from 'sequelize';

class User extends Model {
    static init(database) {
        return super.init(
            {
                id: {
                    type: DataTypes.UUID,
                    primaryKey: true,
                    defaultValue: Sequelize.UUIDV4,
                },
                email: DataTypes.STRING,
                password: DataTypes.STRING,
                firstname: DataTypes.STRING,
                lastname: DataTypes.STRING,
                city: DataTypes.STRING,
                avatar: DataTypes.STRING
            }, {
                tableName: 'User',
                sequelize: database,
            },
        );
    }
}

export default User;
