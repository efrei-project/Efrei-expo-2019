import { config as dotEnvConfig } from 'dotenv';

dotEnvConfig();

module.exports = {
    development: {
        username: process.env.BDD_USER,
        password: process.env.BDD_PASSWORD,
        database: process.env.BDD_DBNAME,
        host: process.env.BDD_HOST,
        dialect: 'postgres',
        dialectOptions: {
            ssl: true
        }
    },
    production: {
        databaseUrl: process.env.DATABASE_URL,
        dialect: 'postgres',
        dialectOptions: {
            ssl: true
        }
    },
};



