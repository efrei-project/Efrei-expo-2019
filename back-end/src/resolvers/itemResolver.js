import { Item, User } from '../database/models';

const itemResolver = {
    Query: {
        items: async () => {
            return (await Item.findAll({
                include: {
                    model: User,
                    attributes: { exclude: ['password'] },
                },
            })).map(e => e.get({ plain: true }));
        },
    },
    Mutation: {
        createItem: async (_, args) => {            
            const { name, description, price, userId, imageUrl, category } = args.data;

            var item = await Item.create({
                name,
                description,
                price,
                imageUrl,
                category,
                userId,
            });
            return item;
        },

        editItem: async (_, args) => {
            const { id, name, price, userId, imageURL, category } = args.data;

            const updatedItem = Item.update(
                { name, price, userId, imageURL, category },
                {
                    returning: true,
                    where: { id }
                }
            );

            return updatedItem;
        },

        deletedItem: async (obj, args, ctx, info) => {
            const { id } = args.id;
            Item.destroy({
                where: { id }
            })

            return !!check;
        }
    },
};

export default itemResolver;
