import { User, Item } from '../database/models';
import { AuthenticationError } from 'apollo-server';

const userResolver = {
    Query: {
        users: async () => {
            return User.findAll();
        },

        user: async (_, args) => {
            const id = args.id;

            const user = await User.findByPk(id);
            const items = (await Item.findAll({
                where: {
                    userId: id
                },
                include: {
                    model: User,
                    attributes: { exclude: ['password'] },
                },
            })).map(e => e.get({ plain: true }));

            return {
                user: user,
                itemsSells: items
            }
        },

        login: async (_, args) => {
            const { email, password } = args.data
            const user = await User.findOne({ where: { email, password } });
            if (!user) throw new AuthenticationError('Email or password wrong !');
            return user;
        },
    },
    Mutation: {
        createUser: async (_, args) => {
            const { email, password, firstname, lastname, city, avatar } = args.data;
            const newUser = {
                id: id,
                email: email,
                password: password,
                firstname: firstname,
                lastname: lastname,
                city: city,
                avatar: avatar
            };
            return await User.create(newUser)
        },

        editUser: async (_, args) => {
            const { id, data } = args
            const { email, firstname, lastname, city, avatar } = data;

            await User.update(
                { email, firstname, lastname, city, avatar },
                { where: { id } }
            );
            
            return await User.findByPk(id);
        },

        deleteUser: async (_, args) => {
            const { id } = args.id;
            User.destroy({
                where: { id }
            })

            return !!check;
        }
    }
}

export default userResolver;
