import userSchema from './userSchema';
import itemSchema from './itemSchema';

const baseSchema = `
    type Query
    type Mutation
    
    schema {
        query: Query
        mutation: Mutation
    }
`;

export {
    baseSchema,
    userSchema,
    itemSchema
}
