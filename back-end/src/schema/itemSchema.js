const itemSchema = `
  extend type Query {
    items: [Item!]!
  }

  extend type Mutation {
    createItem(data: ItemInput!): Item!
    editItem(id: ID!, data: ItemInput): Item!
    deletedItem(id: ID!): Boolean!
  }

  type Item {
    id: ID!
    name: String!
    description: String!
    price: Float!
    imageUrl: String
    category: String!
    User: User
  }

  input ItemInput {
    name: String!
    description: String!
    price: Float!
    imageUrl: String!
    category: String!
    userId: ID!
  }
`;

export default itemSchema;
