const userSchema = `
  extend type Query {
    users: [User!]!
    user(id: ID!): UserOutput!
    login(data: LoginInput!): User!
  }

  extend type Mutation {
    createUser(data: UserInput!): User!
    editUser(id: ID!, data: EditUserInput!): User!
    deleteUser(id: ID!): Boolean!
  }

  type User {
    id: ID!
    email: String!
    password: String
    firstname: String!
    lastname: String!
    city: String!
    avatar: String
  }

  input UserInput {
    email: String!
    password: String!
    firstname: String!
    lastname: String!
    city: String!
    avatar: String
  }

  input EditUserInput {
    email: String!
    firstname: String!
    lastname: String!
    city: String!
    avatar: String
  }

  type UserOutput {
    user: User!
    itemsSells: [Item]
  }

  input LoginInput {
    email: String
    password: String
  }
`;

export default userSchema;
