import React, { useState } from 'react'
import { AppLoading } from 'expo'
import { Asset } from 'expo-asset'
import * as Font from 'expo-font'
import { mapping, light as lightTheme } from '@eva-design/eva';
import { ApplicationProvider } from 'react-native-ui-kitten';
import ApolloClient from 'apollo-boost'
import { ApolloProvider } from '@apollo/react-hooks'
import AppContainer from './src/AppContainer'

const client = new ApolloClient({
  uri: 'https://efreibook.herokuapp.com',
})

const App = ({ skipLoadingScreen }) => {
  const [isLoadingComplete, setLoadingComplete] = useState(false)

  async function loadResourcesAsync() {
    await Promise.all([
      Asset.loadAsync([
        require('./assets/icon.png'),
        require('./assets/splash.png'),
      ]),
      // Font.loadAsync({
      //   'Rubik-Black': require('./node_modules/@shoutem/ui/fonts/Rubik-Black.ttf'),
      // }),
    ])
  }

  if (!isLoadingComplete && !skipLoadingScreen) {
    return (
      <AppLoading
        startAsync={loadResourcesAsync}
        onFinish={() => setLoadingComplete(true)}
      />
    )
  }
  return (
    <ApolloProvider client={client}>
      <ApplicationProvider
        mapping={mapping}
        theme={lightTheme}>
        <AppContainer />
      </ApplicationProvider>
    </ApolloProvider>
  )
}

export default App