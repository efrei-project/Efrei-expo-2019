import React from 'react'
import { createAppContainer, createSwitchNavigator } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { Ionicons } from '@expo/vector-icons';
import { RegisterScreen, LoginScreen, ProfileScreen, EditUserScreen } from './screens/User'
import { AddProductScreen, ProductListScreen, ProductScreen } from './screens/Product'
import FilterModal from './components/FilterModal';

const ProductStack = createStackNavigator({
    ProductListScreen: {
        screen: ProductListScreen
    },
    ProductScreen: {
        screen: ProductScreen
    },
    FilterScreen: {
        screen: FilterModal
    },
    AddProductScreen: {
        screen: AddProductScreen
    }
}, {
    initialRouteName: 'ProductListScreen',
    defaultNavigationOptions: {
        header: null
    }
})

const UserStack = createStackNavigator({
    UserScreen: {
        screen: ProfileScreen
    },
    EditUserScreen: {
        screen: EditUserScreen // TODO: Use correct screen
    }
}, {
    initialRouteName: 'UserScreen',
    defaultNavigationOptions: {
        header: null
    }
})

const HomeNav = createBottomTabNavigator({
    ProductStack: {
        screen: ProductStack,
        navigationOptions: {
            title: 'Products',
            tabBarIcon: ({ tintColor }) => {
                return <Ionicons name='ios-list' size={30} color={tintColor} />
            },
        },
    },
    UserStack: {
        screen: UserStack,
        navigationOptions: {
            title: 'User',
            tabBarIcon: ({ tintColor }) => {
                return <Ionicons name='ios-person' size={28} color={tintColor} />
            },
        },
    },
}, {
    initialRouteName: 'ProductStack',
    activeColor: '#f0edf6',
    inactiveColor: '#3e2465',
    barStyle: { backgroundColor: '#694fad' },

})

const LoginStack = createSwitchNavigator({
    RegisterScreen,
    LoginScreen,
    HomeNav,
}, {
    initialRouteName: 'LoginScreen',
});

const AppContainer = createAppContainer(LoginStack);

export default AppContainer;