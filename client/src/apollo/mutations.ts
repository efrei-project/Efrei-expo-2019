import gql from 'graphql-tag';

export const REGISTER_USER = gql`
  mutation registerUser($data: RegisterUserInput!) {
    registerUser(data: $data) {
      id
      email
      firstName
      lastName
      city
    }
  }
`;

export const ADD_PRODUCT = gql`
  mutation createItem($data: ItemInput!) {
    createItem(data: $data) {
      name
      description
      price
      imageUrl
      category
      User {
        id
        email
        firstname
        lastname
        city
        avatar
      }
    }
  }
`;

export const EDIT_USER = gql`
  mutation editUser($id: ID!, $data: EditUserInput!) {
    editUser(id: $id, data: $data) {
      id
      email
      firstname
      lastname
      city
      avatar
    }
  }
`;
