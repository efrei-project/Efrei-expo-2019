import gql from "graphql-tag";

export const ITEMS = gql`
    query {
        items {
            id
            name
            description
            price
            category
            imageUrl
            User {
                id
                firstname
                lastname
                email
                city
                avatar
            }
        }
    }
`

export const LOGIN = gql`
query login($email: String, $password: String) {
    login(
      data: {
        email: $email
        password: $password
      }
    ) {
        id
        firstname
        lastname
        email
        city
        avatar
    }
  }
`

export const USER_PROFILE = gql`
    query user($id: ID!) {
        user(id: $id) {
            user {
                id
                email
                firstname
                lastname
                city
                avatar
            }
            itemsSells {
                id
                name
                description
                price
                imageUrl
                category
                User {
                    id
                    email
                    firstname
                    lastname
                    city
                    avatar
                }
            }
        }
    }
`;