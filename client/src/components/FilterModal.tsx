import React, { useState } from 'react'
import { Layout, Text, CheckBox, Button } from 'react-native-ui-kitten'
import {  View } from 'react-native';
import Modal from 'react-native-modal'

const FilterModal = ({isVisible, onDone, onDismiss}) => {

    const [checked, setChecked] = useState('');

    const onChange = (value) => {
        setChecked(value)
    };

    const data = [
        'Vetements',
        'Vehicule',
        'Velo'
    ]

    return (
        <Modal
            onDismiss={() => onDone(checked)}
            isVisible={isVisible}>
            <View style={{ flex: 1 }}>
                <Layout>
                    <Text category='h6'>Filter by category</Text>
                    {data.map(item => (
                        <CheckBox key={item}
                            checked={item == checked}
                            status='primary'
                            text={item}
                            onChange={() => onChange(item)}
                        />
                    ))}
                </Layout>
                <Button onPress={onDismiss}>Close</Button>
            </View>
        </Modal>
    );
}

export default FilterModal
