import React from 'react'
import { Text } from 'react-native-ui-kitten'

const InputError = ({ error, touched }) => error && touched ? <Text status="danger">{error}</Text> : null

export default InputError
