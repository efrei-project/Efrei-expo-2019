import React from 'react'
import { Layout } from 'react-native-ui-kitten'

const InputMargin = ({ children }) => (
    <Layout style={{ marginBottom: 12.0 }}>
        {children}
    </Layout>
)

export default InputMargin
