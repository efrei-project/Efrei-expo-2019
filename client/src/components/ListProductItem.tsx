import React from 'react'
import Product from '../models/Product'
import { List } from 'react-native-ui-kitten'
import ProductItem from './ProductItem'

const ListProductItem = ({ products, onItemPress }: { products: Product[], onItemPress: Function }) => {   
    const renderItem = ({ item }: { item: Product }) => (
        <ProductItem product={item} onPress={() => onItemPress(item)} />
    )

    return <List data={products} renderItem={renderItem} />
}

export default ListProductItem
