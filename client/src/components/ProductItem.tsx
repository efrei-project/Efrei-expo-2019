import React from 'react'
import Product from '../models/Product'
import { Image, View } from 'react-native'
import { Layout, Text } from 'react-native-ui-kitten'
import { TouchableOpacity } from 'react-native-gesture-handler'

const ProductItem = ({ product, onPress }: { product: Product, onPress: Function }) => {
    return (
        <TouchableOpacity onPress={(_) => onPress()}>
            <Layout style={{ padding: 16.0, flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                <Image source={{ uri: product.imageUrl }} style={{ width: 50.0, height: 50.0, borderRadius: 10 }} />
                <View style={{ width: 16.0 }} />
                <Layout style={{ flex: 1 }}>
                    <Text style={{ fontWeight: '500' }}>{product.name}</Text>
                    <Text>{product.description}</Text>
                </Layout>
                <Text style={{ fontWeight: '500' }}>{`${product.price}€`}</Text>
            </Layout>
        </TouchableOpacity>
    )
}

export default ProductItem
