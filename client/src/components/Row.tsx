import React from 'react';
import { View } from 'react-native';

const Row = ({
  style, justifyContent, alignItems, children
}) => (
  <View
    style={{
      ...style,
      flex: 1,
      justifyContent,
      flexDirection: 'row',
      alignItems
    }}
  >
    {children}
  </View>
);

export default Row