import React from 'react'
import { ScrollView, View } from 'react-native'
import { Text, Layout } from 'react-native-ui-kitten'
import { AntDesign } from '@expo/vector-icons';
import { TouchableOpacity } from 'react-native-gesture-handler';

const TitledScreen = ({
    title,
    icon=null,
    onPress=null,
    contentPadding = 16.0,
    titlePadding = 16.0,
    children,
}) => {
    return (
        <ScrollView style={{ flex: 1 }}>
            <View style={{ marginBottom: 24.0, marginTop: 54.0, flexDirection: "row", justifyContent: "space-between", alignContent: "center",  marginHorizontal: titlePadding }}>
                <Text category="h2">
                    {title}
                </Text>
                {icon && onPress && (
                    <TouchableOpacity onPress={onPress}>
                        <AntDesign name={icon} size={24} />
                    </TouchableOpacity>
                )}
            </View>
            <Layout style={{ padding: contentPadding }}>
                {children}
            </Layout>
        </ScrollView>
    )
}

export default TitledScreen
