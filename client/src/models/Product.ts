import User from './User';

export default class Product {
    id: string
    name: string
    description: string
    price: number
    imageUrl: string
    category: string
    User: User

    constructor(id: string, name: string, description: string, price: number, imageUrl: string, category: string, User: User) {
        this.id = id
        this.name = name
        this.description = description
        this.price = price
        this.imageUrl = imageUrl
        this.category = category
        this.User = User
    }
}
