export default class User {
    id: string
    firstname: string
    lastname: string
    email: string
    city: string
    avatar: string

    constructor(id: string, firstname: string, lastname: string, email: string, city: string, avatar: string) {
        this.id = id
        this.firstname = firstname
        this.lastname = lastname
        this.email = email
        this.city = city
        this.avatar = avatar
    }
}
