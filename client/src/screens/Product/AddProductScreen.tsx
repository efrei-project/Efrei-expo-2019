import React, { useState, useEffect } from 'react'
import { Picker, Image } from 'react-native'
import { Layout, Input, Button, Text } from 'react-native-ui-kitten'
import * as ImagePicker from 'expo-image-picker';
import { Formik } from 'formik'
import * as Yup from 'yup'
import Constants from 'expo-constants';
import * as Permissions from 'expo-permissions';
import TitledScreen from '../../components/TitledScreen'
import InputMargin from '../../components/InputMargin'
import InputError from '../../components/InputError'
import { useMutation } from '@apollo/react-hooks'
import { ADD_PRODUCT } from '../../apollo/mutations'

const AddProductScreen = () => {
    const [addProduct, {
        loading: mutationLoading,
        error: mutationError,
        data: mutationData
    }] = useMutation(ADD_PRODUCT);

    const [image, setImage] = useState(null)

    useEffect(() => {
        getPermissionAsync()
    }, [])

    const onSubmit = async ({ name, description, category, price }) => {
        await addProduct({
            variables: {
                data: {
                    name,
                    description,
                    category,
                    price: parseFloat(price),
                    imageUrl: image || "",
                    userId: "3a679d34-d3ae-11e9-aec2-172a4627723d"
                }
            },
        });
    }

    const validationSchema = Yup.object().shape({
        name: Yup.string().required('Required'),
        description: Yup.string().min(20, 'Minimum 20 characters').required('Required'),
        price: Yup.number().min(0, 'Price must be greater than 0 !').required('Required'),
        category: Yup.string().required('Required'),
    })

    const getPermissionAsync = async () => {
        if (Constants.platform.ios) {
            const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
            if (status !== 'granted') {
                alert('Sorry, we need camera roll permissions to make this work!');
            }
        } else {
            const { status } = await Permissions.askAsync(Permissions.CAMERA);
            if (status !== 'granted') {
                alert('Sorry, we need camera permissions to make this work!');
            }
        }
    }

    const pickImage = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.All,
            allowsEditing: true,
            aspect: [4, 3],
            base64: true,
        });
        if (!result.cancelled) {
          setImage(result.uri);
        }
    };

    const takePhoto = async () => {
        let result = await ImagePicker.launchCameraAsync({
            base64: true,
        })
        if (!result.cancelled) {
          setImage(result.uri);
        }
    }

    return (
        <TitledScreen title="Add product">
            <Formik
                validateOnChange={true}
                validateOnBlur={true}
                enableReinitialize={true}
                validationSchema={validationSchema}
                initialValues={{
                    name: '',
                    description: '',
                    price: '',
                    category: '',
                }}
                onSubmit={onSubmit}
            >
                {({ values, errors, handleChange, handleBlur, touched, handleSubmit }) => (
                    <Layout>
                        <InputMargin>
                            <Input
                                placeholder="Product name"
                                size='large'
                                status={errors.name && touched.name ? 'danger' : null}
                                value={values.name}
                                onChangeText={handleChange('name')}
                                onBlur={handleBlur('name')}
                            />
                            <InputError error={errors.name} touched={touched.name} />
                        </InputMargin>

                        <InputMargin>
                            <Input
                                placeholder="Product description"
                                size='large'
                                multiline
                                status={errors.description && touched.description ? 'danger' : null}
                                value={values.description}
                                onChangeText={handleChange('description')}
                                onBlur={handleBlur('description')}
                            />
                            <InputError error={errors.description} touched={touched.description} />
                        </InputMargin>

                        <InputMargin>
                            <Input
                                placeholder="Product price"
                                size='large'
                                status={errors.price && touched.price ? 'danger' : null}
                                value={values.price}
                                onChangeText={handleChange('price')}
                                onBlur={handleBlur('price')}
                            />
                            <InputError error={errors.price} touched={touched.price} />
                        </InputMargin>

                        <Text>Category</Text>
                        <Picker
                            selectedValue={values.category}
                            onValueChange={handleChange('category')}>
                            <Picker.Item label="Automobile" value="automobile" />
                            <Picker.Item label="Vêtement" value="vetement" />
                            <Picker.Item label="Vélo" value="velo" />
                        </Picker>

                        {image && <Image source={{ uri: image }} style={{ width: 50, height: 50 }} />}
                        <Button style={{ marginTop: 10.0 }} onPress={pickImage}>
                            Add image gallery
                        </Button>
                        <Button style={{ marginTop: 10.0 }} onPress={takePhoto}>
                            take picture
                        </Button>

                        <Button style={{ marginTop: 16.0 }} onPress={(_) => handleSubmit()} disabled={mutationLoading}>
                            Add product
                        </Button>
                    </Layout>
                )}
            </Formik>
        </TitledScreen>
    )
}

export default AddProductScreen
