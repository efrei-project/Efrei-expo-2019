import React, { useState } from 'react';
import { ActivityIndicator } from 'react-native';
import TitledScreen from '../../components/TitledScreen';
import Product from '../../models/Product';
import ListProductItem from '../../components/ListProductItem';
import { Button, Layout } from 'react-native-ui-kitten';
import FilterModal from '../../components/FilterModal';
import { useQuery } from '@apollo/react-hooks';
import { ITEMS } from '../../apollo/queries';

const ProductListScreen = ({ navigation }) => {
    const { error, loading, data } = useQuery(ITEMS);

    const onItemPress = (product: Product) => {
        navigation.push('ProductScreen', { product })
    };

    const [visible, setVisible] = useState(false);

    const showDialog = () => setVisible(true);
    const hideDialog = () => setVisible(false);

    return (
        <Layout style={{ flex: 1 }}>
            {loading || !data
                ? <ActivityIndicator />
                : (
                    <TitledScreen title="LeBonAngle" icon='filter' onPress={(v) => showDialog()} contentPadding={0}>
                        <FilterModal isVisible={visible} onDone={(value) => { }} onDismiss={hideDialog} />
                        <ListProductItem products={data.items} onItemPress={onItemPress} />
                    </TitledScreen>
                )
            }
            <Button onPress={(_) => navigation.push('AddProductScreen')} style={{ position: 'absolute', right: 16.0, bottom: 16.0, borderRadius: 50.0 }}>+</Button>
        </Layout>
    );
};

export default ProductListScreen