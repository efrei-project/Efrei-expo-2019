import React from 'react'
import { Image, View } from 'react-native'
import { Text, Avatar, Button, Layout } from 'react-native-ui-kitten';
import LayoutConstant from '../../constants/LayoutConstant';
import Product from '../../models/Product';
import { Ionicons } from '@expo/vector-icons';
import { TouchableOpacity } from 'react-native-gesture-handler';

const ProductScreen = ({ navigation }) => {
    const product = navigation.getParam('product', null) as Product;

    if (!product) navigation.goBack()

    return (
        <Layout>
            <Image
                source={{ uri: product.imageUrl }}
                style={
                    {
                        height: LayoutConstant.window.height * 0.33,
                        width: '100%',
                        backgroundColor: 'transparent'
                    }}
                resizeMode="cover"
            />
            <View style={{ position: 'absolute', zIndex: 99, left: 16.0, top: 28.0 }}>
                <TouchableOpacity onPress={(v) => navigation.goBack()}>
                    <Ionicons name="ios-arrow-round-back" size={50} color="#000" />
                </TouchableOpacity>
            </View>
            <Layout style={{ padding: 16.0 }}>
                <View style={{ flexDirection: 'row', alignItems: "center" }}>
                    <Text category='h4' style={{ flex: 1 }}>{product.name}</Text>
                    <Text category='h5'>{`${product.price} €`}</Text>
                </View>

                <Text category='label'>{product.category}</Text>
                <Text style={{ marginTop: 16.0, marginBottom: 32.0 }}>{product.description}</Text>

                <View style={{ height: 1, backgroundColor: '#F0F0F0' }} />
                <View style={{ flexDirection: 'row', alignItems: "center", marginVertical: 10.0 }}>
                    <Avatar source={{ uri: product.User.avatar || 'https://www.marketingmuses.com/wp-content/uploads/2018/01/invis-user.png' }} />
                    <Text category='s1' style={{ marginHorizontal: 16.0 }}>
                        {`${product.User.firstname} ${product.User.lastname}`}
                    </Text>
                </View>
                <View style={{ height: 1, backgroundColor: '#F0F0F0' }} />

                <Button style={{ marginTop: 32.0, borderRadius: 50.0 }} onPress={(v) => { }}>Achat</Button>
            </Layout>
        </Layout>
    )
};

export default ProductScreen;