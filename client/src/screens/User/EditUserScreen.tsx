import React from 'react'
import { Image, View, TouchableOpacity } from 'react-native'
import { Layout, Input, Button, Text } from 'react-native-ui-kitten'
import { useQuery, useMutation } from '@apollo/react-hooks';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { EDIT_USER } from '../../apollo/mutations';
import TitledScreen from '../../components/TitledScreen';
import InputMargin from '../../components/InputMargin';
import InputError from '../../components/InputError'

const EditUserScreen = ({ navigation }) => {

    const user = navigation.getParam('user')

    const [editUser, {
        loading: mutationLoading,
        error: mutationError,
        data: mutationData
    }] = useMutation(EDIT_USER);

    const onSubmit = async ({ firstname, lastname, email, city, avatar}) => {
        await editUser({
            variables: {
                id: user.id,
                data: {
                    email,
                    firstname,
                    lastname,
                    city,
                    avatar
                }
            },
        });
        navigation.goBack()
    }

    const validationSchema = Yup.object().shape({
        email: Yup.string().email('Invalid email').required('Required'),
        firstname: Yup.string().required('Required'),
        lastname: Yup.string().required('Required'),
        city: Yup.string().required('Requiered'),
        avatar: Yup.string().required('Requiered'),
    })

    return (
        <TitledScreen title='Edit Profile'>
            <Formik
                validateOnChange={true}
                validateOnBlur={true}
                enableReinitialize={true}
                validationSchema={validationSchema}
                initialValues={{
                    firstname: `${user.firstname}`,
                    lastname: `${user.lastname}`,
                    email: `${user.email}`,
                    city: `${user.city}`,
                    avatar: `${user.avatar}`,
                }}
                onSubmit={onSubmit}
            >
                {({ values, errors, handleChange, handleBlur, touched, handleSubmit }) => (
                    <Layout>
                        <View style={{ marginBottom:15.0, alignItems: "center" }}>
                        <Image
                            source={{ uri: user.avatar === '' ? 'https://www.marketingmuses.com/wp-content/uploads/2018/01/invis-user.png' : user.avatar }}
                            style={
                                {
                                    height: 125,
                                    width: 125,
                                    backgroundColor: 'transparent',
                                    borderRadius: 125 / 2,
                                }}
                        />
                        </View>
                        <InputMargin>
                            <Input
                                placeholder="Firstname"
                                size='large'
                                status={errors.firstname && touched.firstname ? 'danger' : null}
                                value={values.firstname}
                                onChangeText={handleChange('firstname')}
                                onBlur={handleBlur('firstname')}
                            />
                            <InputError error={errors.firstname} touched={touched.firstname} />
                        </InputMargin>

                        <InputMargin>
                            <Input
                                placeholder="Lastname"
                                size='large'
                                multiline
                                status={errors.lastname && touched.lastname ? 'danger' : null}
                                value={values.lastname}
                                onChangeText={handleChange('lastname')}
                                onBlur={handleBlur('lastname')}
                            />
                            <InputError error={errors.lastname} touched={touched.lastname} />
                        </InputMargin>
                        <InputMargin>
                            <Input
                                placeholder="Email"
                                size='large'
                                status={errors.email && touched.email ? 'danger' : null}
                                value={values.email}
                                onChangeText={handleChange('email')}
                                onBlur={handleBlur('email')}
                            />
                            <InputError error={errors.email} touched={touched.email} />
                        </InputMargin>
                        <InputMargin>
                            <Input
                                placeholder="City"
                                size='large'
                                status={errors.city && touched.city ? 'danger' : null}
                                value={values.city}
                                onChangeText={handleChange('city')}
                                onBlur={handleBlur('city')}
                            />
                            <InputError error={errors.city} touched={touched.city} />
                        </InputMargin>
                        <InputMargin>
                            <Input
                                placeholder="Avatar"
                                size='large'
                                status={errors.avatar && touched.avatar ? 'danger' : null}
                                value={values.avatar}
                                onChangeText={handleChange('avatar')}
                                onBlur={handleBlur('avatar')}
                            />
                            <InputError error={errors.avatar} touched={touched.avatar} />
                        </InputMargin>
                        <Button style={{ marginTop: 16.0 }} onPress={(_) => handleSubmit()} disabled={mutationLoading}>Modify</Button>
                    </Layout>
                )}
            </Formik>
        </TitledScreen>
    )
};

export default EditUserScreen;
