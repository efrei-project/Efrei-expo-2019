import React, { useEffect } from 'react'
import { Alert } from 'react-native'
import { useLazyQuery } from '@apollo/react-hooks'
import { Formik } from 'formik';
import { Input, Button, Layout, Text } from 'react-native-ui-kitten'
import * as Yup from 'yup'
import InputError from '../../components/InputError';
import InputMargin from '../../components/InputMargin';
import TitledScreen from '../../components/TitledScreen';
import { LOGIN } from '../../apollo/queries';

const LoginScreen = ({ navigation }) => {
  const [getLogin, { loading, error, data }] = useLazyQuery(LOGIN)

  useEffect(()=> {
    if (data && data.login) {
      navigation.navigate('HomeNav')
    }

    if (error) {
      return Alert.alert(
        'Whoops !',
        error.graphQLErrors[0].message,
        [
          { text: 'Try again' }
        ],
        { cancelable: false },
      );
    }
  }, [data, error, loading])

  const onSubmit = async ({ email, password }) => {
    await getLogin({
      variables: { email, password },
    })
  }

  const validationSchema = Yup.object().shape({
    email: Yup.string().email('Invalid email').required('Required'),
    password: Yup.string().min(4, 'Minimum 4 characters').required('Required'),
  })

  return (
    <TitledScreen title="Login">
      <Formik
        validateOnChange={true}
        validateOnBlur={true}
        enableReinitialize={true}
        validationSchema={validationSchema}
        initialValues={{
          email: 'test@test.com',
          password: 'test',
        }}
        onSubmit={onSubmit}
      >
        {({ handleBlur, handleChange, handleSubmit, values, errors, touched }) => (
          <Layout>
            <InputMargin>
              <Input
                placeholder="Your email"
                size='large'
                status={errors.email && touched.email ? 'danger' : null}
                value={values.email}
                onChangeText={handleChange('email')}
                onBlur={handleBlur('email')}
              />
              <InputError error={errors.email} touched={touched.email} />
            </InputMargin>

            <InputMargin>
              <Input
                placeholder="Your password"
                size='large'
                status={errors.password && touched.password ? 'danger' : null}
                value={values.password}
                secureTextEntry
                onChangeText={handleChange('password')}
                onBlur={handleBlur('password')}
              />
              <InputError error={errors.password} touched={touched.password} />
            </InputMargin>

            <Button onPress={(_) => handleSubmit()}>Login</Button>
          </Layout>
        )}
      </Formik>
      <Button onPress={(_) => navigation.navigate('RegisterScreen')} style={{ marginTop: 35.0 }} appearance="outline">
        Don't have an account ? Register
      </Button>
    </TitledScreen>
  )
};

export default LoginScreen;