import React from 'react'
import { Image, View, TouchableOpacity, ActivityIndicator } from 'react-native'
import { Text } from 'react-native-ui-kitten';
import { EvilIcons, MaterialIcons } from '@expo/vector-icons';
import Product from '../../models/Product';
import { ScrollView } from 'react-native-gesture-handler';
import ListProductItem from '../../components/ListProductItem';
import Row from '../../components/Row';
import { useQuery } from '@apollo/react-hooks';
import { USER_PROFILE } from '../../apollo/queries';

const ProfileScreen = ({ navigation }) => {
    const { error, loading, data } = useQuery(USER_PROFILE, {
        variables: {
            id: "3a672800-d3ae-11e9-aec2-172a4627723d" //TODO: Use real user profile
        }
    });

    const onItemPress = (product: Product) => {
        navigation.push('ProductScreen', { product })
    };

    if (loading || !data) return <ActivityIndicator />

    const { user, itemsSells } = data.user

    return (
        <ScrollView>
            <View style={{ marginTop: 60.0, alignItems: "center" }}>
                <Row justifyContent="space-between">
                    <Image
                        source={{ uri: user.avatar || 'https://www.marketingmuses.com/wp-content/uploads/2018/01/invis-user.png' }}
                        style={{
                            height: 125,
                            width: 125,
                            backgroundColor: 'transparent',
                            borderRadius: 125 / 2,
                        }}
                    />
                    <TouchableOpacity onPress={() => navigation.navigate("EditUserScreen", { user })}>
                        <MaterialIcons name='edit' size={22} />
                    </TouchableOpacity>
                </Row>
                <Text category='h6'>
                    {`${user.firstname} ${user.lastname}`}
                </Text>
                <Row alignItems='center'>
                    <EvilIcons name='location' size={22} />
                    <Text category='s1'>
                        {`${user.city}`}
                    </Text>
                </Row>
            </View>
            <ListProductItem products={itemsSells} onItemPress={onItemPress} />
        </ScrollView>
    )
};

export default ProfileScreen;