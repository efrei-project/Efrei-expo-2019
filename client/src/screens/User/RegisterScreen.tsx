import React from 'react'
import { Input, Button, Layout } from 'react-native-ui-kitten';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { REGISTER_USER } from '../../apollo/mutations'
import { useMutation } from '@apollo/react-hooks'
import InputError from '../../components/InputError';
import InputMargin from '../../components/InputMargin';
import TitledScreen from '../../components/TitledScreen';

const RegisterScreen = ({ navigation }) => {
    const [registerUser, {
        loading: mutationLoading,
        error: mutationError,
        data: mutationData
    }] = useMutation(REGISTER_USER);

    const onSubmit = async ({ email, password, firstName, lastName, city }) => {
        await registerUser({
            variables: {
                data: {
                    email,
                    password,
                    firstName,
                    lastName,
                    city,
                }
            },
        });
    }

    const validationSchema = Yup.object().shape({
        email: Yup.string().email('Invalid email').required('Required'),
        password: Yup.string().min(4, 'Minimum 4 characters').required('Required'),
        firstName: Yup.string().required('Required'),
        lastName: Yup.string().required('Required'),
        city: Yup.string().required('Requiered'),
    })

    return (
        <TitledScreen title="Register">
            <Formik
                validateOnChange={true}
                validateOnBlur={true}
                enableReinitialize={true}
                validationSchema={validationSchema}
                initialValues={{
                    email: '',
                    password: '',
                    firstName: '',
                    lastName: '',
                    city: '',
                }}
                onSubmit={onSubmit}
            >
                {({ values, errors, handleChange, handleBlur, touched, handleSubmit }) => (
                    <Layout>
                        <InputMargin>
                            <Input
                                placeholder="Your email"
                                size='large'
                                status={errors.email && touched.email ? 'danger' : null}
                                value={values.email}
                                onChangeText={handleChange('email')}
                                onBlur={handleBlur('email')}
                            />
                            <InputError error={errors.email} touched={touched.email} />
                        </InputMargin>

                        <InputMargin>
                            <Input
                                placeholder="Your password"
                                size='large'
                                status={errors.password && touched.password ? 'danger' : null}
                                secureTextEntry
                                value={values.password}
                                onChangeText={handleChange('password')}
                                onBlur={handleBlur('password')}
                            />
                            <InputError error={errors.password} touched={touched.password} />
                        </InputMargin>

                        <InputMargin>
                            <Input
                                placeholder="Your firstname"
                                size='large'
                                status={errors.firstName && touched.firstName ? 'danger' : null}
                                value={values.firstName}
                                onChangeText={handleChange('firstName')}
                                onBlur={handleBlur('firstName')}
                            />
                            <InputError error={errors.firstName} touched={touched.firstName} />
                        </InputMargin>

                        <InputMargin>
                            <Input
                                placeholder="Your lastname"
                                size='large'
                                status={errors.lastName && touched.lastName ? 'danger' : null}
                                value={values.lastName}
                                onChangeText={handleChange('lastName')}
                                onBlur={handleBlur('lastName')}
                            />
                            <InputError error={errors.lastName} touched={touched.lastName} />
                        </InputMargin>

                        <InputMargin>
                            <Input
                                placeholder="Your city"
                                size='large'
                                status={errors.city && touched.city ? 'danger' : null}
                                value={values.city}
                                onChangeText={handleChange('city')}
                                onBlur={handleBlur('city')}
                            />
                            <InputError error={errors.city} touched={touched.city} />
                        </InputMargin>

                        <Button style={{ marginTop: 16.0 }} onPress={(_) => handleSubmit()} disabled={mutationLoading}>Register</Button>
                    </Layout>
                )}
            </Formik>
            <Button onPress={(_) => navigation.navigate('LoginScreen')} style={{ marginTop: 32.0 }} appearance="outline">
                Already have an account ? Login
            </Button>
        </TitledScreen>
    )
}

export default RegisterScreen;